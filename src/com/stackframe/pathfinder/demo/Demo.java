// Copyright (C) 2002-2010 StackFrame, LLC http://www.stackframe.com/
// This software is provided under the GNU General Public License, version 2.
package com.stackframe.pathfinder.demo;

import com.stackframe.pathfinder.PathFinder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.Component;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.ServiceLoader;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;

public class Demo extends JApplet {

    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    private Map map;
    private MapView mapView;
    private final JButton startButton = new JButton("Find Path");
    private Location start;
    private Location goal;
    private JComboBox algo;
    private static final String idle = "PathFinder idle.";
    private final JLabel status = new JLabel(idle);
    private long startTime;
    private static final int bounds = 32;
    private Future<List<Location>> futurePath;

    private void handleDone(final List<Location> path) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                long stopTime = System.currentTimeMillis();
                mapView.setPath(path);
                String message;
                if (path == null) {
                    message = "No path found in ";
                } else {
                    message = "Path of " + path.size() + " steps found in ";
                    for(Location loc : path) {
                        System.out.println(loc.getX() + "," + loc.getY());
                    }
                }

                message += (stopTime - startTime) + " milliseconds.";
                
                status.setText(message);
                mapView.setEnabled(true);
                algo.setEnabled(true);
                startButton.setText("Find Path");
            }

        });
    }

    private void doNewSearch() {
        startButton.setText("Abort");
        algo.setEnabled(false);
        mapView.setEnabled(false);
        status.setText("PathFinder working ...");
        mapView.setPath(null);
        startTime = System.currentTimeMillis();
        futurePath = executor.submit(new Callable<List<Location>>() {

            public List<Location> call() {
                PathFinder<Location> pathfinder = (PathFinder<Location>)algo.getSelectedItem();
                final List<Location> path = pathfinder.findPath(map.getLocations(), start, Collections.singleton(goal));
                handleDone(path);
                return path;
            }

        });
    }

    public Demo() {
        map = new Map(bounds);
        getContentPane().setLayout(new BorderLayout());
        
        Random rand = new Random();
        start = map.getLocation(rand.nextInt(map.getXSize() - 1), rand.nextInt(map.getYSize() - 1));
        goal = map.getLocation(rand.nextInt(map.getXSize() - 1), rand.nextInt(map.getYSize() - 1));
        mapView = new MapView(map, start, goal);
        mapView.setBorder(new BevelBorder(BevelBorder.LOWERED));
        getContentPane().add(mapView, BorderLayout.CENTER);
        getContentPane().add(status, BorderLayout.SOUTH);
        JPanel controlBox = new JPanel();
        controlBox.add(startButton);
        getContentPane().add(controlBox, BorderLayout.NORTH);
        startButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionEvent) {
                if (startButton.getText().equals("Find Path")) {
                    doNewSearch();
                } else {
                    PathFinder pathFinder = (PathFinder)algo.getSelectedItem();
                    pathFinder.cancel();
                    futurePath.cancel(true);
                }
            }

        });

        Vector<PathFinder<Location>> algorithms = new Vector<PathFinder<Location>>();
        ServiceLoader<PathFinder> pathFinders = ServiceLoader.load(PathFinder.class);
        for (PathFinder p : pathFinders) {
            addAlgorithm((PathFinder<Location>)p, algorithms);
        }

        algo = new JComboBox(algorithms);
        algo.setRenderer(new DefaultListCellRenderer() {

            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                PathFinder pathFinder = (PathFinder)value;
                return super.getListCellRendererComponent(list, pathFinder.name(), index, isSelected, cellHasFocus);
            }

        });
    }

    private void addAlgorithm(PathFinder<Location> pathFinder, Vector<PathFinder<Location>> algorithms) {
        algorithms.add(pathFinder);
    }

    public static void main(String[] args) {
        final Demo demo = new Demo();
        JFrame frame = new JFrame("PathFinder");

        frame.getContentPane().add(demo);
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
