package net.railscoder.map;

import java.util.Random;

public class GameMapGenerator {
	
	public static int[][] generate(int seed) {
		float[][] noise = new float[32][32];
		int[][] grass = new int[32][32];
		int[][] collision = new int[32][32];
		int[][] noncollision = new int[32][32];
		int[][] noncollision_aboveplayer = new int[32][32];

		boolean[][] canWriteCollision = new boolean[32][32];
		boolean[][] canWriteNonCollision = new boolean[32][32];
		
		boolean iCanHazBuilding = true;
		
		for(int i = 0; i < grass.length; i++) {
			for(int j = 0; j < grass.length; j++) {
				Random rand = new Random();
				
				grass[i][j] = rand.nextInt(3) + 1;
				canWriteCollision[i][j] = true;
				canWriteNonCollision[i][j] = true;
			}
		}
		
		if(seed == -1) {
			noise = Noise.midpointDisplacement(5, 1, 1, .2f, 1, true, 0);
		} else {
			noise = Noise.midpointDisplacement(5, 1, 1, .2f, 1, false, seed);
		}
		
		for(int i = 0; i < noise.length - 1; i++) {
			for(int j = 0; j < noise.length - 1; j++) {
				if(canWriteCollision[i][j]) {
					float val = Math.abs(noise[i][j]);
					
					if(val > .2 && val <= .22) {
						collision[i][j] = 11;
					} else if(val > .22 && val <= .24) {
						collision[i][j] = 11;
					} else {
						collision[i][j] = 0;
					}
				}
			}
		}
		
		for(int i = 0; i < noise.length - 1; i++) {
			for(int j = 0; j < noise.length - 1; j++) {
				if(canWriteNonCollision[i][j]) {
					float val = Math.abs(noise[i][j]);
					
					if(val <= .2) {
						noncollision[i][j] = 4;
						noncollision_aboveplayer[i][j] = 9;
					} else if(val >= .65 && iCanHazBuilding) {
						try {
							if(i >= 4 && i <= 25) {
								collision[i - 4][j - 2] = 361;
								collision[i - 4][j - 1] = 362;
								collision[i - 4][j] = 363;
								collision[i - 4][j + 1] = 364;
								collision[i - 4][j + 2] = 365;
								collision[i - 3][j - 2] = 369;
								collision[i - 3][j - 1] = 370;
								collision[i - 3][j] = 371;
								collision[i - 3][j + 1] = 372;
								collision[i - 3][j + 2] = 373;
								collision[i - 2][j - 2] = 377;
								collision[i - 2][j - 1] = 378;
								collision[i - 2][j] = 379;
								collision[i - 2][j + 1] = 380;
								collision[i - 2][j + 2] = 381;
								collision[i - 1][j - 2] = 385;
								collision[i - 1][j - 1] = 386;
								collision[i - 1][j] = 387;
								collision[i - 1][j + 1] = 388;
								collision[i - 1][j + 2] = 389;
								collision[i][j - 2] = 393;
								collision[i][j - 1] = 394;
								noncollision[i][j] = 395; // DOOR
								collision[i][j + 1] = 396;
								collision[i][j + 2] = 397;
								
								// no grass on building kthx
								noncollision_aboveplayer[i - 4][j - 2] = 0;
								noncollision_aboveplayer[i - 4][j - 1] = 0;
								noncollision_aboveplayer[i - 4][j] = 0;
								noncollision_aboveplayer[i - 4][j + 1] = 0;
								noncollision_aboveplayer[i - 4][j + 2] = 0;
								noncollision_aboveplayer[i - 3][j - 2] = 0;
								noncollision_aboveplayer[i - 3][j - 1] = 0;
								noncollision_aboveplayer[i - 3][j] = 0;
								noncollision_aboveplayer[i - 3][j + 1] = 0;
								noncollision_aboveplayer[i - 3][j + 2] = 0;
								noncollision_aboveplayer[i - 2][j - 2] = 0;
								noncollision_aboveplayer[i - 2][j - 1] = 0;
								noncollision_aboveplayer[i - 2][j] = 0;
								noncollision_aboveplayer[i - 2][j + 1] = 0;
								noncollision_aboveplayer[i - 2][j + 2] = 0;
								noncollision_aboveplayer[i - 1][j - 2] = 0;
								noncollision_aboveplayer[i - 1][j - 1] = 0;
								noncollision_aboveplayer[i - 1][j] = 0;
								noncollision_aboveplayer[i - 1][j + 1] = 0;
								noncollision_aboveplayer[i - 1][j + 2] = 0;
								noncollision_aboveplayer[i][j - 2] = 0;
								noncollision_aboveplayer[i][j - 1] = 0;
								noncollision_aboveplayer[i][j] = 0;
								noncollision_aboveplayer[i][j + 1] = 0;
								noncollision_aboveplayer[i][j + 2] = 0;
								iCanHazBuilding = false;
							}
						} catch (ArrayIndexOutOfBoundsException e) {
							
						}
					} else {
						noncollision[i][j] = 0;
					}
				}
			}
		}
		
		// make the portals
		//up
		noncollision[31][14] = 17;
		noncollision[31][15] = 18;
		noncollision[31][16] = 18;
		noncollision[31][17] = 19;
		
		//down
		noncollision[0][14] = 33;
		noncollision[0][15] = 34;
		noncollision[0][16] = 34;
		noncollision[0][17] = 35;
		
		//left
		noncollision[14][0] = 19;
		noncollision[15][0] = 27;
		noncollision[16][0] = 27;
		noncollision[17][0] = 35;
		
		//right
		noncollision[14][31] = 17;
		noncollision[15][31] = 25;
		noncollision[16][31] = 25;
		noncollision[17][31] = 33;
		
		// remove any other non-collision objects from our portal area
		//up
		noncollision_aboveplayer[31][14] = 0;
		noncollision_aboveplayer[31][15] = 0;
		noncollision_aboveplayer[31][16] = 0;
		noncollision_aboveplayer[31][17] = 0;

		//down
		noncollision_aboveplayer[0][14] = 0;
		noncollision_aboveplayer[0][15] = 0;
		noncollision_aboveplayer[0][16] = 0;
		noncollision_aboveplayer[0][17] = 0;

		//left
		noncollision_aboveplayer[14][0] = 0;
		noncollision_aboveplayer[15][0] = 0;
		noncollision_aboveplayer[16][0] = 0;
		noncollision_aboveplayer[17][0] = 0;

		//right
		noncollision_aboveplayer[14][31] = 0;
		noncollision_aboveplayer[15][31] = 0;
		noncollision_aboveplayer[16][31] = 0;
		noncollision_aboveplayer[17][31] = 0;
		
		// remove any collision objects from our portal area
		//up
		collision[31][14] = 0;
		collision[31][15] = 0;
		collision[31][16] = 0;
		collision[31][17] = 0;
		
		// down
		collision[0][14] = 0;
		collision[0][15] = 0;
		collision[0][16] = 0;
		collision[0][17] = 0;
		
		//left
		collision[14][0] = 0;
		collision[15][0] = 0;
		collision[16][0] = 0;
		collision[17][0] = 0;
		
		//right
		collision[14][31] = 0;
		collision[15][31] = 0;
		collision[16][31] = 0;
		collision[17][31] = 0;
		
		StringBuilder mapData = new StringBuilder();
		mapData.append(printHeader(32, 32));
		mapData.append(printLayer(grass, "Grass"));
		mapData.append(printLayer(noncollision, "Non-Collision"));
		mapData.append(printLayer(collision, "Collision"));
		mapData.append(printLayer(noncollision_aboveplayer, "Non-Collision-Above-Player"));
		mapData.append("</map>");
		
		//return mapData.toString();
        return collision;
	}
	
	private static String printLayer(int[][] grass, String layerName) {
		StringBuilder printLayer = new StringBuilder();
		
		printLayer.append("<layer name=\"" + layerName + "\" width=\"" + grass.length + "\" height=\"" + grass.length + "\">");
		printLayer.append("<data encoding=\"csv\">");
		for(int i = 0; i < grass.length; i++) {
			String layerLine = "";
			for(int j = 0; j < grass[i].length; j++) {
				layerLine += (grass[i][j]);
				
				if(i == 31 && j == 31) {
					// I don't know why it's being bitch, but it doesn't work if I reverse the conditions and leave out the else
				} else {
					layerLine += (",");
				}
				
			}
			printLayer.append(layerLine);
		}
		
		printLayer.append("</data>");
		printLayer.append("</layer>");
		
		return printLayer.toString();
	}
	
	private static String printHeader(int height, int width) {
		StringBuilder printLayer = new StringBuilder();
		
		printLayer.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		printLayer.append("<map version=\"1.0\" orientation=\"orthogonal\" width=\"" + width + "\" height=\"" + height + "\" tilewidth=\"16\" tileheight=\"16\">");
		printLayer.append("<tileset firstgid=\"1\" name=\"game\" tilewidth=\"16\" tileheight=\"16\">");
		printLayer.append("<image source=\"game.png\" width=\"128\" height=\"800\"/>");
		printLayer.append("<tile id=\"9\">");
		printLayer.append("<properties>");
		printLayer.append("<property name=\"blocked\" value=\"true\"/>");
		printLayer.append("</properties>");
		printLayer.append("</tile>");
		printLayer.append("</tileset>");
		
		return printLayer.toString();
	}

}